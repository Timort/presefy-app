var sync = null;
var sid = null;
var channelName = null;
var channelActive = null;
var preseShowBtnCliked = false;
var selectedSlide = null;
var preseData = null;
var firstConn = false;

var layout = {
	main: {
		login: {
			open: function() {
				$('.conn-sensor').hide();
				$('.header-ref-btn').attr('class', 'icon header-ref-btn notLogged ion-person-add clickable');
				layout.main.login.inputError('hide');
				layout.main.login.serverError('hide');
				$('.title.main-title').html('Вход')
				$('.header-ref-btn').show();
				$('.window').hide();
				$('.window.login').show();
			},

			/*enableSpinner: function () {
				$('.spinner-container.login').show();
				$('#login-form').css('opacity', '0.7');
				$('.log-in-btn').attr('disabled', 'true');
				$('#login-form input').attr('disabled', 'true');
			},

			disableSpinner: function () {
				$('.spinner-container').hide();
				$('#login-form').css('opacity', '1');
				$('.log-in-btn').removeAttr('disabled');
				$('#login-form input').removeAttr('disabled', 'true');
			},*/

			inputFilter: function (username, password, onSuccess) {
				if (username === '') {
					layout.main.login.inputError('show', 'username', 'Поле "Логин или Email" не может быть пустым.');
				} else if (password === '') {
					layout.main.login.inputError('show', 'password', 'Поле "Пароль" не может быть пустыми.');
				} else {
					onSuccess();
				}
			}, 

			inputError: function (action, type, message) {
				switch(action) {
					case 'show':
						switch(type) {
							case 'username':
								$('.input-error.l-username-error p').html(message);
								$('.input-error.l-username-error').show('slide');
								$('.window.login .list .item.item-input.l-username').addClass('on-input-error');
								break;
							case 'password':
								$('.input-error.l-password-error p').html(message);
								$('.input-error.l-password-error').show('slide');
								$('.window.login .list .item.item-input.l-password').addClass('on-input-error');
						}
						break;
					case 'hide':
						$('.window.login .input-error').hide();
						$('.window.login .list label').removeClass('on-input-error');
				}
			},

			serverError: function (action, message) {
				switch(action) {
					case 'show':
						$('.server-error.login p').html(message);
						$('.server-error.login').show('slide');
						break;
					case 'hide':
						$('.server-error.login').hide('slide');
				}
			}
		},

		register: {
			open: function () {
				$('.register-success-padding').hide();
				$('.header-ref-btn').attr('class', 'icon header-ref-btn notLogged ion-log-in clickable');
				layout.main.register.inputError('hide');
				layout.main.register.serverError('hide');
				$('.title.main-title').html('Регистрация')
				$('.header-ref-btn').show();
				$('.window').hide();
				$('.window.register').show();
			},

			/*enableSpinner: function () {
				$('.spinner-container.register').show();
				$('#register-form').css('opacity', '0.7');
				$('.register-btn').attr('disabled', 'true');
				$('#regiter-form input').attr('disabled', 'true');
			},

			disableSpinner: function () {
				$('.spinner-container.register').hide();
				$('#register-form').css('opacity', '1');
				$('.register-btn').removeAttr('disabled');
				$('#regiter-form. input').removeAttr('disabled');
			},*/

			inputFilter: function (username, email, password, onSuccess) {
				if (username === '') {
					layout.main.register.inputError('show', 'username', 'Поле "Имя пользователя" не может быть пустым.');
					return;
				} else {
					var check = /[^\w.-]/.exec(username);
					if (check !== null) {
						layout.main.register.inputError('show', 'username', 'Недопустимый символ - "' + check[0] + '". Допустимые символы: a-z A-Z 0-9 . - _.');
						return;
					}
				}
				if (password === '') {
					layout.main.register.inputError('show', 'password', 'Поле "Пароль" не может быть пустым.');
					return;
				}
		  	var email = email;
		 		if (email === "") {
		 			layout.main.register.inputError('show', 'email','Поле "Email" не может быть пустым.');
		 		} else {
		 			if (/@/g.test(email)) {
				 		email = email.split('@');
				 		if (email.length === 2) {
				 			if (email[0] === "") {
					 			layout.main.register.inputError('show', 'email', 'Перед символом "@" должен находится хотя бы 1 символ. Формат адреса: example@example.com.');
					 		} else {
					 			email[0] = /[^\w.-]/.exec(email[0]);
					 			if (email[0] === null) {
					 				if (email[1] === "") {
					 					layout.main.register.inputError('show', 'email', 'После "@" должны находится символы. Формат адреса: example@example.com.');
					 				} else {
					 					check = /[^a-zA-Z.]/g.exec(email[1]);
					 					if (check === null) {
					 						check = /\./g.exec(email[1]);
					 						if (check === null) {
					 							layout.main.register.inputError('show', 'email','После символа "@" должна находится как минимум одна ".". Формат адреса: example@example.com.');
					 						} else {
					 							check = email[1].split('.');
					 							if (check[0] === "") {
					 								layout.main.register.inputError('show', 'email','Символ "." не должен следовать сразу за симолом "@". Формат адреса: example@example.com.');
					 							} else {
					 								for (email[1] = 1; email[1] < check.length; email[1]++) {
					 									if (check[email[1]].length < 2) {
					 										layout.main.register.inputError('show', 'email', 'После симмвола "." должны следовать как минимум 2 символа. Формат адреса: example@example.com.')
					 										return;
					 									} 
					 								}
					 								onSuccess();
					 							}
					 						}
					 					} else {
					 						layout.main.register.inputError('show', 'email', 'После "@" не может находится "' + check + '". Допустимые символы: a-z A-Z 0-9 .. Формат адреса: example@example.com.');
					 					}
					 				}
					 			} else {
					 				layout.main.register.inputError('show', 'email','Перед "@" не может находится: "' + email[0] + '". Допустимые символы: a-z A-Z 0-9 . - _. Формат адреса: example@example.com.');
					 			}
					 		}
				 		} else {
				 			layout.main.register.inputError('show', 'email','Адрес не может содержать более одного символа "@". Формат адреса: example@example.com.');
				 		}
			 		} else {
			 			layout.main.register.inputError('show', 'email','В адресе должен присутсвовать символ "@". Формат адреса: example@example.com.');
			 		}
			 	}
			},

			inputError: function (action, type, message) {
				switch(action) {
					case 'show':
						switch(type) {
							case "username":
								$('.input-error.r-username-error p').html(message);
								$('.input-error.r-username-error').show('slide');
								$('.window.register .list .item.item-input.r-username').addClass('on-input-error');
								break;
							case "email":
								$('.input-error.email-error p').html(message);
								$('.input-error.email-error').show('slide');
								$('.window.register .list .item.item-input.email').addClass('on-input-error');
								break;
							case "password":
								$('.input-error.r-password-error p').html(message);
								$('.input-error.r-password-error').show('slide');
								$('.window.register .list .item.item-input.r-password').addClass('on-input-error');
						}
						break;
					case 'hide':
						$('.input-error').hide('slide');
						$('.window.register .list label').removeClass('on-input-error');
				}
			},

			serverError: function (action, message) {
				switch(action) {
					case 'show':
						$('.server-error.register p').html(message);
						$('.server-error.register').show('slide');
						break;
					case 'hide':
						$('server-error.register').hide('slide');
				}
			}
		},

		options: function() {
			$('.title.main-title').html('Настройки');
			$('.window').hide();
			$('.header-ref-btn').hide();
			$('.window.options').show();
			$('#side-menu-content').css('margin-bottom', '0px');
			$('.side-menu-footer').hide().css('z-index', '0');
			layout.sideMenu.controllingOff();
		},

		dashboard: function() {
			$('.title.main-title').html('Презентации');
			$('.window').hide();
			$('.window.dashboard').show();
			$('#side-menu-content').css('margin-bottom', '44px');
			$('.side-menu-footer').show().css('z-index', '10');
			layout.sideMenu.controllingOff();
		},

		slidesList: function() {
			$('.title.main-title').html('Список слайдов');
			$('.window').hide();
			$('#side-menu-content').css('margin-bottom', '0px');
			$('.side-menu-footer').hide().css('z-index', '0');
			$('.window.slides-list').show();
			layout.sideMenu.controllingOff();
		},

		controlling: function() {
			$('.title.main-title').html('Управление');
			$('.bar.prese-title-bar').show();
			$('.window').hide();
			$('.window.controlling').show();
			$('#side-menu-content').css('margin-bottom', '0px');
			$('.side-menu-footer').hide().css('z-index', '0');
			layout.sideMenu.controllingOn();
		}, 

		globalSpinner: function (action, message) {
			switch(action) {
				case 'show':
					$('#g-spinner-content-container p').html(message);
					$('#global-spinner').show();
					break;
				case 'hide':
					$('#g-spinner-content-container p').html('');
					$('#global-spinner').hide();
			}
		},

		alert: {
			show: function (message, btnsAmount, btnsContentArray, functionsArray) {
				$('#alert-content-container .button-bar a').remove();
				$('#alert-content-container p').html(message);
				for (var i = 0; i < btnsAmount; i++ ) {
					$('#alert-content-container .button-bar').append('<a class="button">' + btnsContentArray[i] + '</a>');
				}
				for (var i = 0; i < btnsAmount; i++) {
					$('#alert-content-container a').eq(i).on('click', functionsArray[i]);
				}
				$('#alert').show();
			},

			hide: function () {
				$('#alert').hide();
			}
		}
	},

	sideMenu: {
		controllingOn: function() {
			$('.controlling').show();
			if (channelActive === false) {
				$('.controlling.prese-nav.dropdown').hide();
			}
		},

		controllingOff: function() {
			$('.controlling').hide();
		}
	},

	header: {
		connSet: function () {
			$('.icon.ion-connection-bars').fadeIn();
			$('.icon.ion-ios-close-empty').fadeOut();
		}, 

		connLost: function () {
			$('.icon.ion-ios-close-empty').fadeIn();
			$('.conn-state')
		},
	}, 

	state: {
		logged: function () {
			$('#side-menu-content').css('margin-top', '44px');
			$('.notLogged').hide();
			$('.logged').show();
		}, 

		notLogged: function () {
			$('#side-menu-content').css('margin-top', '0px');
			$('.logged').hide();
			$('.notLogged').show();
		}
	}
}

var action = {
	login: function(username, password) {
		layout.main.globalSpinner('show', 'Авторизация...');
		$.ajax({
		  url: "https://presefy.com/v2/account",
		  type: "DELETE",
		  success: function() {
		  var rData = '{"username":"' + username + '","password":"' + password + '"}';
			$.ajax({
				url: "https://presefy.com/v2/account", 
				type: "POST",
				data: rData,
				dataType : "json",
				timeout: 7000,
				beforeSend: function( xhr ) {
				  xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
				  xhr.setRequestHeader('Content-Length', '' + rData.length + '');
				},
				success: function( json, status, xhr ) {
					var httpReq = new plugin.HttpRequest();
					httpReq.post("https://presefy.com/v2/account", {
					  username: username,
					  password: password
					}, function(err, data) {
						sid = data;
						action.onLogin(json.username, json.presentations.current, json.presentations.max);
					});
				},
				error: function( xhr, status, errorThrown ) {
					if (xhr.status === 422 || xhr.status === 500 || status === 'timeout') {
						layout.main.login.serverError('show', 'Неверное имя пользователя или пароль.');
					} else {
						layout.main.login.serverError('show', 'Не удалось авторизоваться.');
					}
					layout.main.globalSpinner('hide');
				}
			});
		  },
		  error: function(xhr, status, errorThrown) {
		 		layout.main.globalSpinner('hide');
		 		layout.main.login.serverError('show', 'Не удалось авторизоваться. Проверте соединение с интернетом.');
		  }
		});
	},

	register: function (username, email, password) {
		layout.main.globalSpinner('show', 'Регистрация...');
		$.ajax({
			url: "https://presefy.com/v2/account",
			type: "DELETE",
			success: function() {
				var rData = '{"username":"' + username + '","password":"' + password + '","email":"' +  email + '"}';
				$.ajax({
					url: "https://presefy.com/v2/users", 
					type: "POST",
					data: rData,
					dataType : "json",
					beforeSend: function( xhr ) {
						xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
						xhr.setRequestHeader('Content-Length', '' + rData.length + '');
					},
					success: function( json, status, xhr ) {
						var httpReq = new plugin.HttpRequest();
						httpReq.post("https://presefy.com/v2/account", {
						  username: username,
						  password: password
						}, function(err, data) {
							if (err === null) {
								sid = data;
								action.onLogin(json.username, json.presentations.current, json.presentations.max);
							} else {
								layout.main.register.serverError('show', 'Зарегистрироватся не удалось, ошибка: ' + err + '');
							}
						});
					},
					error: function( xhr, status, errorThrown ) {
						layout.main.globalSpinner('hide');
						if (xhr.status === 422) {
							if (JSON.parse(xhr.responseText).invalids.email === 'Email taken') {
								layout.main.register.inputError('show', 'email', 'Email уже используется');
							} else if (JSON.parse(xhr.responseText).invalids.email === 'Invalid email address') {
								layout.main.register.inputError('show', 'email', 'Неверный формат Email адреса');
							} else if (JSON.parse(xhr.responseText).invalids.username === 'Username taken') {
								layout.main.register.inputError('show', 'username', 'Имя пользователя занято');
							} 
						} else {
							layout.main.register.serverError('show', 'Зарегистрироватся не удалось, ошибка: ' + errorThrown + '');
						}
					}
				})
			},
			error: function(xhr, status, errorThrown) {
				layout.main.globalSpinner('hide');
				layout.main.register.serverError('show', 'Не удалось зарегистрироваться, проверте соединение с интернетом. Ошибка: ' + errorThrown + '');
			}
		})
	},

	onLogin: function (username, current, max) {
		layout.main.globalSpinner('show', 'Получение информации...');
		channelName = username;
		$('.side-menu-title').html(username);
		$('.side-menu-footer p:nth-child(2)').html(current + ' из ' + max);
		$('#link p').html('Сраница демонстрации: presefy.com/' + username);
		$('.window.controlling.container #iframe').append('<iframe style="width: full" scrolling="no" seamless="true" src="https://presefy.com:443/#/channels/' + username + '?embed" frameborder="0"></iframe>');
		$.ajax({
			url: "https://presefy.com/v2/presentations?own=", 
			type: "GET",
			success: function( json, status, xhr ) {
				preseData = action.parsePreseData(json);
				action.createPrese(json);
				layout.state.logged();
				layout.main.dashboard();
				action.connect();
				layout.main.globalSpinner('hide');
			},
			error: function( xhr, status, errorThrown ) {
				layout.main.globalSpinner('hide');
				layout.main.alert.show('Не удалось получить информацию. Проверьте соединение с интернетом', 2, ['Закрыть', 'Попробовать заново'], [
					function () {
						layout.main.alert.hide();
					},
					function () {
						layout.main.alert.hide();
						action.refreshPreseList();
					}
				]);
			}
		});
	},

	createPrese: function(json) {
		$('.item.prese-details').remove();
		$('.side-menu-prese-row').remove();
		for (var i = 0; i < json.length; i++) {
			switch(json[i].conversion.state) {
				case 'converted':
					$('.window.dashboard.container').append('<a class="item item-thumbnail-left item-button-right prese-details" id="' + i +'"> <img class="thumbnail" src="' + json[i].imgSlides[0].thumbnail_url.https + '"> <h2 class="prese-title">' + json[i].name + '</h2> <button class="button prese-show-btn" id="'+ i +'"> <i class="icon ion-play"></i> </button> </a>');
					$('.side-menu-accordion.prese-accordion .list-converted').append('<div class="row side-menu-prese-row"> <div class="col col-75 side-menu-prese-col"> <button class="button side-menu-btn prese-btn" id="' + i + '"> <p class="side-menu-text">' + json[i].name + '</p> </button> </div> <div class="col side-menu-prese-col"> <button menu-toggle="left" class="button button-icon icon-center ion-play side-menu-btn side-menu-prese-show-btn" id="' + i + '"> </button> </div> </div>');
					break;
				case 'converting':
					$('.window.dashboard.container').append('<a class="item item-thumbnail-left item-button-right prese-details" style="pointer-events: none"> <div id="converting-thumbnail"> <i class="icon ion-ios-crop"></i> <i class="icon ion-ios-help-empty"></i> </div> <h2 class="prese-title">' + json[i].name + '</h2> <button class="button prese-show-btn"> <i class="icon ion-load-a"></i> <p>Конвертация...</p> </button> </a>'); 
					$('.side-menu-accordion.prese-accordion .list-converting').append('<div class="row side-menu-prese-row" style="pointer-events=none"> <div class="col col-75 side-menu-prese-col"> <button class="button side-menu-btn prese-btn"> <p class="side-menu-text">' + json[i].name + '</p> </button> </div> <div class="col side-menu-prese-col"> <button menu-toggle="left" class="button button-icon icon-center ion-ios-more side-menu-btn side-menu-prese-show-btn" style="font-size: 30px"> </button> </div> </div>');
			}
		}
	},

	refreshPreseList: function () {
		layout.main.globalSpinner('show', 'Получение информации...');
		sync.connect();
		$.ajax({
			url: "https://presefy.com/v2/presentations?own=", 
			type: "GET",
			success: function( json, status, xhr ) {
				preseData = action.parsePreseData(json);
				action.createPrese(json);
				layout.main.globalSpinner('hide');
			},
			error: function( xhr, status, errorThrown ) {
				layout.main.globalSpinner('hide');
				layout.main.alert.show('Не удалось получить информацию. Проверьте соединение с интернетом', 2, ['Закрыть', 'Попробовать заново'], [
					function () {
						layout.main.alert.hide();
					},
					function () {
						layout.main.alert.hide();
						action.refreshPreseList();
					}
				]);
			}
		});
	},

	logout: function() {
		layout.main.globalSpinner('show', 'Выход...');
		$.ajax({
		  url: "https://presefy.com/v2/account",
		  type: "DELETE",
		  success: function() {
		  	sync.disconnect();
		  	sync.removeAllListeners();
		  	sync = null;
		  	$('iframe').remove();
				$('.item.prese-details').remove();
				$('.side-menu-prese-row').remove();
				$('#link p').html('');
				layout.main.globalSpinner('hide');
				layout.state.notLogged();
		  	layout.sideMenu.controllingOff();
		    layout.main.login.open();
		  },
		  error: function() {
		  	layout.main.globalSpinner('hide');
		    alert("error");
		  }
		});
	},

	connect: function() {
		var preseIndex = null;
    var presentationId = null;
    var slideChange = 0;
    var currentSlide = null;
    var options = {
        path:'/v2/socket.io',
        reconnection: true,
        query: {
            sessionId: sid
        }
    };
	  sync = io.connect('https://presefy.com:443' + '/sync', options);

		sync.on('connect', function () {
			if (!firstConn) {
				firstConn = true;
			}
			layout.header.connSet();

			sync.on('subscribed', function (event, state, controlling) {
				if (state.prese === null) {
					channelActive = false;
					$('.controlling-btn.enabled').removeClass('enabled').attr('disabled', 'true');
					$('.control-panel.gd').attr('disabled', 'true');
				} else {
					$('.control-panel.gd').removeAttr('disabled');
					channelActive = true;
					presentationId = state.prese;
					currentSlide = state.slide;
					for (var i = 0; i < preseData.length; i++) {
						if (state.prese === preseData[i].id) {
							preseIndex = i;
							$('.title.prese-title').html(preseData[preseIndex].name);
							$('#cm-slide-box').html(currentSlide + '/' + preseData[preseIndex].slidesAmount);
							action.createSideMenuSlidesList(preseIndex);
							action.createStepsList(preseIndex, state.slide, state.step);
							// mark current step
							$('.button.side-menu-btn.step-btn').eq(0).addClass('current').attr('disabled', 'true');
							// mark current slide
		 					$('.button.side-menu-btn.slide-btn').eq(state.slide-1).addClass('current').attr('disabled', 'true');
		 					// mark current presentation
		 					$('.row.side-menu-prese-row').eq(preseIndex).addClass('current');
							break;
						}
					}
				}
				sync.off('subscribed');
				sync.on('subscribed', function (event, state, controlling) {
					if (slideChange === -1) {
						slide = state.slide - 1;
				  	sync.emit('show', channelName, presentationId, slide, 1);
					}  else {
						if (state.step === preseData[preseIndex].steps[state.slide-1]) {
							slide = state.slide + 1;
					  	sync.emit('show', channelName, presentationId, slide, 1);
						} else {
							slide = state.step + 1;
							sync.emit('show', channelName, presentationId, state.slide, slide);
						}
					}
				});
			});

			$('.window.dashboard').on('click', 'button.prese-show-btn',  function() {
				preseShowBtnCliked = true;
				preseIndex = $(this).attr('id');
				presentationId = preseData[preseIndex].id;
				$('.title.prese-title').html(preseData[preseIndex].name);
				action.createSideMenuSlidesList(preseIndex);
				sync.emit('show', channelName, presentationId, 1, 1);
				layout.sideMenu.controllingOn();
				layout.main.controlling();
			});

			$('.side-menu-accordion.prese-accordion').on('click', 'button.side-menu-prese-show-btn', function() {
				preseIndex = $(this).attr('id');
				presentationId = preseData[preseIndex].id;
				$('.title.prese-title').html(preseData[preseIndex].name);
				$('.side-menu-accordion.prese-accordion').hide('slide');
				$('.icon.dropdown-icon.prese').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
				action.createSideMenuSlidesList(preseIndex);
				sync.emit('show', channelName, presentationId, 1, 1);
				layout.sideMenu.controllingOn();
				layout.main.controlling();
			});

			$('.button.slides-list.show-btn').click(function() {
				preseIndex = $(this).attr('id');
				presentationId = preseData[preseIndex].id;
				$('.title.prese-title').html(preseData[preseIndex].name);
				action.createSideMenuSlidesList(preseIndex);
				sync.emit('show', channelName, presentationId, selectedSlide, 1);
				layout.sideMenu.controllingOn();
				layout.main.controlling();
			});

			// Controlling buttons handlers (start over, end, next, prev etc.)
			$('.row.control-panel.subrow')
				.on('click', '.btn-prev', function () {
					slideChange = -1;
					sync.emit('subscribe', channelName);
				})
				.on('click', '.btn-next', function () {
					slideChange = 1;
					sync.emit('subscribe', channelName);
				})
				.on('click', '.btn-begin', function () {
					sync.emit('show', channelName, presentationId, 1, 1);
				})
				.on('click', '.btn-to-end', function () {
					sync.emit('show', channelName, presentationId, preseData[preseIndex].slidesAmount, 1);
				})
				.on('click', '.btn-slides', function () {
					action.createSlidesList(preseIndex, currentSlide);
					layout.main.slidesList();
				})
				.on('click', '.btn-end', function () {
					sync.emit('clear', channelName);
				});

		$('.side-menu-accordion.slides-accordion').on('click', 'button.side-menu-btn.slide-btn', function () {
			sync.emit('show', channelName, presentationId, $(this).attr('id'), 1);
			$('.icon.dropdown-icon.slides').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
			$('.side-menu-accordion.slides-accordion').toggle('slide');
		});

		$('.side-menu-accordion.steps-accordion').on('click', '.button.side-menu-btn.step-btn', function () {
			sync.emit('show', channelName, presentationId, currentSlide, $(this).attr('id'));
			$('.icon.dropdown-icon.steps').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
			$('.side-menu-accordion.steps-accordion').toggle('slide');
		});

  		sync.on('update', function (channelId, state) {
  			if (state.prese === null) {
  				channelActive = false;
  				$('#cm-slide-box').html('0/0');
  				$('.controlling-btn.prese-nav.enabled').removeClass('enabled').attr('disabled', 'true');
  				$('.prese-nav.dropdown').hide('slide');
  				$('.control-panel.ld').attr('disabled', 'true');
  				// unmark all presentations
  				$('.row.side-menu-prese-row').removeClass('current');
  			} else if (channelActive === false) {
  				channelActive = true;
  				currentSlide = state.slide;
  				$('#cm-slide-box').html(currentSlide + '/' + preseData[preseIndex].slidesAmount);
  				action.createStepsList(preseIndex, state.slide, state.step);
  				$('.controlling-btn').addClass('enabled').removeAttr('disabled');
  				$('.control-panel.gd').removeAttr('disabled');
  				$('.side-menu-subheader.prese-nav.dropdown').show('slide');
  				// mark current step 
  				$('.button.side-menu-btn.step-btn').removeClass('current').removeAttr('disabled');
  				$('.button.side-menu-btn.step-btn').eq(0).addClass('current').attr('disabled', 'true');
  				// mark current slide
  				$('.button.side-menu-btn.slide-btn').removeClass('current').removeAttr('disabled');
		 			$('.button.side-menu-btn.slide-btn').eq(state.slide-1).addClass('current').attr('disabled', 'true');
		 			// mark current presentation
		 			$('.row.side-menu-prese-row').removeClass('current');
					$('.row.side-menu-prese-row').eq(preseIndex).addClass('current');
  			} else {
  				currentSlide = state.slide;
  				$('#cm-slide-box').html(currentSlide + '/' + preseData[preseIndex].slidesAmount);
  				action.createStepsList(preseIndex, state.slide, state.step);
  				// mark current step 
  				$('.button.side-menu-btn.step-btn').removeClass('current').removeAttr('disabled');
  				$('.button.side-menu-btn.step-btn').eq(0).addClass('current').attr('disabled', 'true');
  				// mark current slide
  				$('.button.side-menu-btn.slide-btn').removeClass('current').removeAttr('disabled');
		 			$('.button.side-menu-btn.slide-btn').eq(state.slide-1).addClass('current').attr('disabled', 'true');
		 			// mark current presentation
		 			$('.row.side-menu-prese-row').removeClass('current');
					$('.row.side-menu-prese-row').eq(preseIndex).addClass('current');
  			}
  		});

			sync.emit('subscribe', channelName);
	  });

  	sync.on('disconnect', function() {
  		layout.header.connLost();
  		layout.main.alert.show('Потеряна связь с сервером. Проверьте соединение с интернетом. Используйте кнопку "Обновить" в верхней части экрана, чтобы установить соединение снова', 1, ['ОК'], [
					function () {
						layout.main.alert.hide();
					}
				]);
  		$('.window.dashboard').off('click', 'button.prese-show-btn');
  		$('.side-menu-accordion.prese-accordion').off('click', 'side-menu-prese-show-btn');
  		$('.button.slides-list.show-btn').off();
			$('.button.button-balanced.next').off('click');
			$('.button.button-balanced.previous').off('click');
			$('.row.controlling-btns-row').off('click');
			$('.side-menu-accordion.slides-accordion').off();
  	});

  	if (firstConn){
			sync.io.opts.query.sessionId = sid;
			sync.connect();
  	}
	},

	createStepsList: function (preseIndex, slide, step) {
		$('.button.side-menu-btn.step-btn').remove();
		$('.col.control-panel.dropdown .list.steps-list .item').remove();
		for (var i = step; i <= preseData[preseIndex].steps[slide-1]; i++) {
			$('.side-menu-accordion.steps-accordion').append('<button class="button side-menu-btn step-btn" id="' + i + '"> <p class="side-menu-text">Шаг ' + i + '</p> </button>');
		}
	},

	createSlidesList: function(preseIndex, currentSlide) {
		$('.row.slides-list.main-row').remove();
		$('.button.slides-list.show-btn').attr('id', preseIndex);
		for (var i = 0; i < Math.ceil(preseData[preseIndex].slidesAmount/4); i++) {
			$('.window.slides-list').append('<div class="row responsive-lg slides-list main-row"> <div class="row responsive-sm slides-list subrow"> <div class="col slides-list"> </div> <div class="col slides-list"> </div> </div> <div class="row responsive-sm slides-list"> <div class="col slides-list"> </div> <div class="col slides-list"> </div> </div>'); }

		for (var i = 0; i < preseData[preseIndex].slidesAmount; i++) {
			$('.col.slides-list').eq(i).append('<div class="slide-index"> <p>' + (i+1) + '</p></div> <img src="' + preseData[preseIndex].urls[i] +'">');
		}

		if (currentSlide === undefined) {
			selectedSlide = null;
			$('.button.slides-list.show-btn').attr('disabled', 'true').removeClass('enabled');
		} else {
			selectedSlide = currentSlide;
			$('.col.slides-list img').eq(currentSlide-1).addClass('selected');
			$('.col.slides-list .slide-index').eq(currentSlide-1).addClass('selected');
		}
	},

	createSideMenuSlidesList: function (preseIndex) {
		$('.button.side-menu-btn.slide-btn').remove();
		$('.col.control-panel.dropdown .list.slides-list .item').remove();
		for (var i = 0; i < preseData[preseIndex].slidesAmount; i++) {
			$('.side-menu-accordion.slides-accordion').append('<button class="button side-menu-btn slide-btn" id="' + (i+1) + '"> <p class="side-menu-text">Слайд ' + (i+1) + '</p> </button>');
		}
	},

	parsePreseData: function (json) {
		function Prese (name, id, slidesAmount, linksArray, stepsArray) {
			this.name = name;
			this.id = id;
			if (id) {
				this.slidesAmount = slidesAmount;
				this.thumbnail_url = linksArray[0].thumbnail_url.https;
				this.urls = [];
				for (var i = 0; i < linksArray.length; i++) {
					this.urls.push(linksArray[i].content_url.https);
				}
				this.steps = [];
				for (var i = 0; i < stepsArray.length; i++) {
					this.steps.push(stepsArray[i].steps);
				}
			}
		}
		var data = [];
		for (var i = 0; i < json.length; i++) {
			switch(json[i].conversion.state) {
				case 'converted':
					var temp = new Prese(json[i].name, json[i].id, json[i].slidego.slides.length, json[i].imgSlides, json[i].slidego.slides);
					break;
				case 'converting':
					var temp = new Prese(json[i].name, null);
			}
			data.push(temp);
		}
		return data;
	} 
}