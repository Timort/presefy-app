angular.module('starter', ['ionic'])

$(document).on('deviceready', function () {
  $('.title.main-title')
    .css('left', '0px')
    .css('right', '0px');
  layout.sideMenu.controllingOff();

  // Header button handlers
  $('.bar.bar-header')
    .on('click', '.icon.ion-person-add', function () {
      layout.main.register.open();
    })
    .on('click', '.icon.ion-log-in', function () {
      layout.main.login.open();  
    })
    .on('click', '.icon.dashboard-refresher', function () {
    	action.refreshPreseList();
    });


  // Login/register handlers
  $('#login-form').submit(function() {
  	layout.main.login.serverError('hide');
  	layout.main.login.inputError('hide');
  	layout.main.login.inputFilter($('input[name=l-username]').val(), $('input[name=l-password]').val(), function () {
    	action.login($('input[name=l-username]').val(), $('input[name=l-password]').val());
  	});
  });

  $('#register-form').submit(function () {
  	layout.main.register.serverError('hide');
  	layout.main.register.inputError('hide');
    layout.main.register.inputFilter($('input[name=r-username]').val(), $('input[name=email]').val(), $('input[name=r-password]').val(), function () {
    	action.register($('input[name=r-username]').val(), $('input[name=email]').val(), $('input[name=r-password]').val());
    });
  });

  // Controlling window handlers
  $('.bar.prese-title-bar')
    .on('click', '#iframe-dropdown-btn', function () {
      var controlPanelContainer = $('#control-panel-container');
      if (controlPanelContainer.css('border-top') === '0px none rgb(0, 0, 0)') {
        controlPanelContainer.css('border-top', '1px solid #a0a0a0');
      } else {
        controlPanelContainer.css('border-top', 'none');
      }
      $('#iframe').toggle('slide');
      $(this).toggleClass('ion-android-arrow-dropright ion-android-arrow-dropdown');
    });


  $('.window.dashboard').on('click', '.item.prese-details', function() {
    if (!preseShowBtnCliked) { 
      action.createSlidesList($(this).attr('id'));
      layout.main.slidesList();
    }
    preseShowBtnCliked = false;
  });

  $('.side-menu-accordion.prese-accordion').on('click', 'button.side-menu-btn.prese-btn', function () {
    action.createSlidesList($(this).attr('id'));
    layout.main.slidesList();
    $('.icon.dropdown-icon.prese').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
    $('.side-menu-accordion.prese-accordion').toggle('slide');
  });

  $('.window.slides-list').on('click', '.col.slides-list', function () {
    if (selectedSlide === null) {
      $('button.slides-list.show-btn').removeAttr('disabled').addClass('enabled');
    }
    selectedSlide = $(this).find('p').html();
    $('.col.slides-list').children().removeClass('selected');
    $(this).children().addClass('selected');
  });

  // Side menu handlers
  $('#side-menu-content')
  .on('click', '.side-menu-btn.log-in-ref-btn', function() {
    layout.main.login.open();
  })
  .on('click', '.side-menu-btn.sign-up-ref-btn', function() {
    layout.main.register.open();
  })
  .on('click', '.side-menu-btn.dashboard-ref-btn', function() {
    layout.main.dashboard();
  })
  .on('click', '.side-menu-btn.log-out-btn', function() {
    action.logout();
  })
  .on('click', '.side-menu-btn.options-ref-btn', function() {
    layout.main.options();
  })
  .on('click', '.side-menu-btn.control-ref-btn', function() {
    layout.main.controlling();
  })
  .on('click', '.bar.side-menu-subheader.steps-header', function() {
    $('.icon.dropdown-icon.steps').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
    $('.side-menu-accordion.steps-accordion').toggle('slide');
  })
  .on('click', '.bar.side-menu-subheader.slides-header', function() {
    $('.icon.dropdown-icon.slides').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
    $('.side-menu-accordion.slides-accordion').toggle('slide');
  })
  .on('click', '.bar.side-menu-subheader.prese-header', function() {
    $('.icon.dropdown-icon.prese').toggleClass('ion-android-arrow-dropright').toggleClass('ion-android-arrow-dropdown');
    $('.side-menu-accordion.prese-accordion').toggle('slide');
  });

  $('#check-locale').click(function () {
  	alert();
  	alert('Locale is ' + navigator.getLocaleName(
  		function (locale) {return locale.value;},
  		function ()	{return 'error';}
  	));
  })
});